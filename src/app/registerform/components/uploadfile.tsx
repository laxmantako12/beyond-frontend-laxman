"use client";
import React from "react";
import UploadVideo from "./uploadfile/uploadVideo";
import UploadDoc from "./uploadfile/uploadDoc";

const uploadfile = () => {
  return (
    <div>
      <div className="p-[25px] lg:p-[60px] mt-[12px] overflow-hidden bg-white rounded-[4px]">
        <h2 className="text-primary text-[14px] font-bold uppercase tracking-[.3em] mb-[20px] lg:mb-[40px]">
          UPLOAD FILE
        </h2>

        <div className="mb-[25px] lg:mb-[50px]">
          <div className="lg:flex -mx-2">
            <div className="lg:basis-1/2 px-2">
              <div className="text-center">
                <UploadVideo />
              </div>
            </div>
            <div className="lg:basis-1/2 px-2">
              <div className="text-center">
                <UploadDoc />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default uploadfile;
