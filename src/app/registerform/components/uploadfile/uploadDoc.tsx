import React from 'react';
import { useDropzone } from 'react-dropzone';
import { FileUpIcon } from 'lucide-react';

const maxSize = 4 * 1024 * 1024; // 4 MB in bytes

function sizeValidator(file: File) {
  if (file.size > maxSize) {
    return {
      code: 'size-too-large',
      message: `File size exceeds ${maxSize / (1024 * 1024)} MB`,
    };
  }
  return null;
}

function UploadDoc() {
  const { acceptedFiles, fileRejections, getRootProps, getInputProps } =
    useDropzone({
      maxFiles: 1,
      validator: sizeValidator,
      accept: {
        'application/pdf': ['.pdf'],
        'application/vnd.openxmlformats-officedocument.presentationml.presentation':
          ['.pptx'],
      },
    });

  const acceptedFileItems = acceptedFiles.map((file: any) => (
    <li key={file.path}>
      {file.path} - {file.size} bytes
    </li>
  ));

  const fileRejectionItems = fileRejections.map(
    ({ file, errors }: { file: any; errors: any }) => (
      <li key={file.path}>
        {file.path} - {file.size} bytes
        <ul>
          {errors.map((error: any) => (
            <li className='text-red-700' key={error.code}>
              {error.message}
            </li>
          ))}
        </ul>
      </li>
    )
  );

  return (
    <div className='border border-dashed border-primary rounded-[9px] lg:p-[60px] bg-primary-extralight'>
      <div {...getRootProps({ className: 'dropzone' })}>
        <input {...getInputProps()} />
        <FileUpIcon
          className='inline mb-2'
          size={60}
          strokeWidth={1}
          fill='#F0B441'
          color='#fff'
        />
        <h4 className='text-grey-normal text-[16px] mb-2.5'>
          Drag and drop to upload Presentaion
        </h4>
        <p className='text-grey-normal text-[13px] leading-none'>
          Upload size upto 4 MB
        </p>
        <p className='text-grey-normal text-[13px] leading-none'>
          Supported format PDF,PPTX
        </p>
      </div>
      <div>
        <ul>{acceptedFileItems}</ul>
        <ul>{fileRejectionItems}</ul>
      </div>
    </div>
  );
}

export default UploadDoc;
