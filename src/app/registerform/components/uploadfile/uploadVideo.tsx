import React from "react";
import { useDropzone } from "react-dropzone";
import { FileUpIcon } from "lucide-react";

const maxSize = 20 * 1024 * 1024; // 20 MB in bytes

function sizeValidator(file) {
  if (file.size > maxSize) {
    return {
      code: "size-too-large",
      message: `File size exceeds ${maxSize / (1024 * 1024)} MB`,
    };
  }
  return null;
}

function uploadVideo(props) {
  const { acceptedFiles, fileRejections, getRootProps, getInputProps } =
    useDropzone({
      maxFiles: 1,
      validator: sizeValidator,
      accept: ["video/mp4", "video/avi"],
    });

  const acceptedFileItems = acceptedFiles.map((file) => (
    <li key={file.path}>
      {file.path} - {file.size} bytes
    </li>
  ));

  const fileRejectionItems = fileRejections.map(({ file, errors }) => (
    <li key={file.path}>
      {file.path} - {file.size} bytes
      <ul>
        {errors.map((error) => (
          <li className="text-red-700" key={error.code}>
            {error.message}
          </li>
        ))}
      </ul>
    </li>
  ));

  return (
    <div className="border border-dashed border-primary rounded-[9px] lg:p-[60px] bg-primary-extralight">
      <div {...getRootProps({ className: "dropzone" })}>
        <input {...getInputProps()} />
        <FileUpIcon
          className="inline mb-2"
          size={60}
          strokeWidth={1}
          fill="#F0B441"
          color="#fff"
        />
        <h4 className="text-grey-normal text-[16px] mb-2.5">
          Drag and drop to upload video
        </h4>
        <p className="text-grey-normal text-[13px] leading-none">
          Upload size up to 20 MB
        </p>
        <p className="text-grey-normal text-[13px] leading-none">
          Supported formats: MP4, AVI
        </p>
      </div>
      <div>
        <ul>{acceptedFileItems}</ul>
        <ul>{fileRejectionItems}</ul>
      </div>
    </div>
  );
}

export default uploadVideo;
