import React from "react";
import { Input } from "@/components/ui/input";
import { Label } from "@/components/ui/label";
import Datepicker from "./datepicker";
import RemoveMember from "./removemember";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import { RadioGroup, RadioGroupItem } from "@/components/ui/radio-group";
import { OctagonAlert } from "lucide-react";
import { Plus } from "lucide-react";
const teamInfo = () => {
  return (
    <div className="p-[25px] lg:p-[60px] mt-[12px] overflow-hidden bg-white rounded-[4px]">
      <h2 className="text-primary text-[14px] font-bold uppercase tracking-[.3em] mb-[20px] lg:mb-[40px]">
        Team information
      </h2>
      <div className="p-[20px] lg:p-[30px] overflow-hidden bg-primary-extralight rounded-[15px]">
        <h3 className="text-secondary text-[15px] font-medium mb-[20px]">
          Team Member 1
        </h3>
        <div className="lg:flex lg:flex-wrap ">
        <div className="lg:basis-1/2 mb-[15px] lg:mb-[30px]">
          <div className="grid w-full max-w-[440px] items-center gap-3">
            <div className="flex items-center justify-between">
              <Label
                className="text-grey-normal text-[16px] font-normal"
                htmlFor="Expertise "
              >
                Number of Team Members Other Than You{" "}
              </Label>
            </div>
            <Select>
              <SelectTrigger className="text-primary-medium p-[16px] text-[15px] bg-white border-primary-ultralight h-[50px] rounded-[9px] w-full">
                <SelectValue placeholder="Select Number of Team Members " />
              </SelectTrigger>
              <SelectContent>
                <SelectItem value="0">0</SelectItem>
                <SelectItem value="1">1 </SelectItem>
                <SelectItem value="2">2 </SelectItem>
                <SelectItem value="3">3</SelectItem>
                <SelectItem value="4">4</SelectItem>
                <SelectItem value="5">5</SelectItem>
              </SelectContent>
            </Select>
          </div>
        </div>
        {/* end  */}
          <div className="lg:basis-1/2 mb-[15px] lg:mb-[30px]">
            <div className="grid w-full max-w-[440px] items-center gap-3">
              <Label
                className="text-grey-normal text-[16px] font-normal"
                htmlFor="name"
              >
                Name
              </Label>
              <Input
                type="name"
                id="name"
                placeholder="Full Name"
                className="placeholder:text-primary-medium p-[16px] text-[15px] bg-white border-primary-ultralight h-[50px] rounded-[9px]"
              />
            </div>
          </div>
          {/* end col 1 */}

          {/* <div className="lg:basis-1/2 mb-[15px] lg:mb-[30px]">
            <div className="grid w-full max-w-[440px] items-center gap-3">
              <Label
                className="text-grey-normal text-[16px] font-normal"
                htmlFor="dob"
              >
                DOB
              </Label>
              <Datepicker />
            </div>
          </div> */}
          {/* end col 2 */}

          <div className="lg:basis-1/2 mb-[15px] lg:mb-[30px]">
            <div className="grid w-full max-w-[440px] items-center gap-3">
              <Label
                className="text-grey-normal text-[16px] font-normal"
                htmlFor="email"
              >
                Email
              </Label>
              <Input
                type="email"
                id="email"
                placeholder="Enter Your Email"
                className="placeholder:text-primary-medium p-[16px] text-[15px] bg-white border-primary-ultralight h-[50px] rounded-[9px]"
              />
            </div>
          </div>
          {/* end col 3 */}
          <div className="lg:basis-1/2 mb-[15px] lg:mb-[30px]">
            <div className="grid w-full max-w-[440px] items-center gap-3">
              <Label
                className="text-grey-normal text-[16px] font-normal"
                htmlFor="number"
              >
                Number
              </Label>
              <Input
                type="number"
                id="number"
                placeholder="+966     Mobile Number"
                className="placeholder:text-primary-medium p-[16px] text-[15px] bg-white border-primary-ultralight h-[50px] rounded-[9px]"
              />
            </div>
          </div>
          {/* end col 4 */}

          <div className="lg:basis-1/2 mb-[15px] lg:mb-[30px]">
            <div className="grid w-full max-w-[440px] items-center gap-3">
              <Label
                className="text-grey-normal text-[16px] font-normal"
                htmlFor="name"
              >
                country  of Residence
              </Label>
              <Input
                type="name"
                id="name"
                placeholder="Enter your city of residence"
                className="placeholder:text-primary-medium p-[16px] text-[15px] bg-white border-primary-ultralight h-[50px] rounded-[9px]"
              />
            </div>
          </div>
          {/* end col 5 */}

          
          <div className="lg:basis-1/2 mb-[15px] lg:mb-[30px]">
            <div className="grid w-full max-w-[440px] items-center gap-3">
              <Label
                className="text-grey-normal text-[16px] font-normal"
                htmlFor="nationality"
              >
                Nationality
              </Label>
              <Select>
                <SelectTrigger className="text-primary-medium p-[16px] text-[15px] bg-white border-primary-ultralight h-[50px] rounded-[9px] w-full">
                  <SelectValue placeholder="Choose Nationality" />
                </SelectTrigger>
                <SelectContent>
                  <SelectItem value="nepali">Nepali</SelectItem>
                  <SelectItem value="hindi">Hindi</SelectItem>
                  <SelectItem value="spanish">Spanish</SelectItem>
                </SelectContent>
              </Select>
            </div>
          </div>
          {/* end col 6 */}
          <div className="lg:basis-1/2 mb-[15px] lg:mb-[30px]">
            <div className="grid w-full max-w-[440px] items-center gap-3">
              <Label
                className="text-grey-normal text-[16px] font-normal"
                htmlFor="nationality"
              >
                Teams’ Combined Areas of Expertise
              </Label>
              <Select>
                <SelectTrigger className="text-primary-medium p-[16px] text-[15px] bg-white border-primary-ultralight h-[50px] rounded-[9px] w-full">
                  <SelectValue placeholder="Choose Expertise" />
                </SelectTrigger>
                <SelectContent>
                  <SelectItem value="0">IoT</SelectItem>
                  <SelectItem value="1">Artificial intelligence (Ai) </SelectItem>
                  <SelectItem value="2">Robotics </SelectItem>
                  <SelectItem value="3">Blockchain  </SelectItem>
                </SelectContent>
              </Select>
            </div>
          </div>
          {/* end  */}
           <div className="lg:basis-1/2 mb-[15px] lg:mb-[30px]">
            <div className="grid w-full max-w-[440px] items-center gap-3">
              <div className="flex items-center justify-between">
                <Label
                  className="text-grey-normal text-[16px] font-normal "
                  htmlFor="Filmathon"
                >
                  How did you hear about the Filmathon?
                </Label>
                {/* <div className="flex items-center gap-1">
                  <OctagonAlert
                    size={24}
                    fill="#F0B441"
                    color="#fff"
                    strokeWidth={2}
                  />
                  <p className="text-[12px] text-grey-light tracking-[-0.01em]">
                    Choose you University from the drop down
                  </p>
                </div> */}
              </div>

              <Select>
                <SelectTrigger className="text-primary-medium p-[16px] text-[15px] bg-white border-primary-ultralight h-[50px] rounded-[9px] w-full">
                  <SelectValue placeholder="Select your Filmathon" />
                </SelectTrigger>
                <SelectContent>
                  <SelectItem value="0">Recommendation </SelectItem>
                  <SelectItem value="1">Private Invitation </SelectItem>
                  <SelectItem value="2">Filmthon Website </SelectItem>
                  <SelectItem value="3">University Invitation  </SelectItem>
                  <SelectItem value="4">Social Media Platforms </SelectItem>
                  <SelectItem value="5">Social Media Influencers </SelectItem>
                </SelectContent>
              </Select>
            </div>
          </div> 
          {/* end col 7 */}
          <div className="lg:basis-1/2 mb-[15px] lg:mb-[30px] lg:mt-[30px]">
            <p className="leading-none mb-[20px] text-grey-normal text-[16px]">
            Have you or any of your team members ever worked on technical projects in the past?
            </p>

            <RadioGroup defaultValue="less-year">
              <div className="flex items-center space-x-2 mb-[20px]">
                <RadioGroupItem value="less-year" id="less-year" />
                <Label
                  className="text-primary-normal font-normal text-[15px]"
                  htmlFor="less-year"
                >
                  Yes
                </Label>
              </div>

              <div className="flex items-center space-x-2 mb-[20px]">
                <RadioGroupItem value="fouryear" id="fouryear" />
                <Label
                  className="text-primary-normal font-normal text-[15px]"
                  htmlFor="fouryear"
                >
                  No
                </Label>
              </div>
              
            </RadioGroup>
          </div>
          {/* end col */}

          <div className="lg:basis-1/2 mb-[15px] lg:mb-[30px] lg:mt-[30px]">
            <p className="leading-none mb-[20px] text-grey-normal text-[16px]">
            Have you or any of your team members ever worked on technical projects related to the film industry in the past?
            </p>

            <RadioGroup defaultValue="less-year">
              <div className="flex items-center space-x-2 mb-[20px]">
                <RadioGroupItem value="less-year" id="less-year" />
                <Label
                  className="text-primary-normal font-normal text-[15px]"
                  htmlFor="less-year"
                >
                  Yes
                </Label>
              </div>

              <div className="flex items-center space-x-2 mb-[20px]">
                <RadioGroupItem value="fouryear" id="fouryear" />
                <Label
                  className="text-primary-normal font-normal text-[15px]"
                  htmlFor="fouryear"
                >
                  No
                </Label>
              </div>
              
            </RadioGroup>
          </div>
          {/* end col */}

          <div className="lg:basis-1/2 mb-[15px] lg:mb-[30px] lg:mt-[30px]">
            <p className="leading-none mb-[20px] text-grey-normal text-[16px]">
            Have you or any of your team members ever participated in any competitions, accelerators, trainings or bootcamp program?
            </p>

            <RadioGroup defaultValue="less-year">
              <div className="flex items-center space-x-2 mb-[20px]">
                <RadioGroupItem value="less-year" id="less-year" />
                <Label
                  className="text-primary-normal font-normal text-[15px]"
                  htmlFor="less-year"
                >
                  Yes
                </Label>
              </div>

              <div className="flex items-center space-x-2 mb-[20px]">
                <RadioGroupItem value="fouryear" id="fouryear" />
                <Label
                  className="text-primary-normal font-normal text-[15px]"
                  htmlFor="fouryear"
                >
                  No
                </Label>
              </div>
              
            </RadioGroup>
          </div>
          {/* end col */}
        </div>
        {/* remove Member  */}
        <RemoveMember />
        {/* remove Member  */}
      </div>
      {/* team 1 */}
      <div className="p-[20px] lg:p-[30px] overflow-hidden bg-primary-extralight rounded-[15px] mt-[12px] ">
        <h3 className="text-secondary text-[15px] font-medium mb-[20px]">
          Team Member 2
        </h3>
        <div className="lg:flex lg:flex-wrap ">
          <div className="lg:basis-1/2 mb-[15px] lg:mb-[30px]">
            <div className="grid w-full max-w-[440px] items-center gap-3">
              <Label
                className="text-grey-normal text-[16px] font-normal"
                htmlFor="name"
              >
                Name
              </Label>
              <Input
                type="name"
                id="name"
                placeholder="Full Name"
                className="placeholder:text-primary-medium p-[16px] text-[15px] bg-white border-primary-ultralight h-[50px] rounded-[9px]"
              />
            </div>
          </div>
          {/* end col 1 */}

          <div className="lg:basis-1/2 mb-[15px] lg:mb-[30px]">
            <div className="grid w-full max-w-[440px] items-center gap-3">
              <Label
                className="text-grey-normal text-[16px] font-normal"
                htmlFor="dob"
              >
                DOB
              </Label>
              <Datepicker />
            </div>
          </div>
          {/* end col 2 */}

          <div className="lg:basis-1/2 mb-[15px] lg:mb-[30px]">
            <div className="grid w-full max-w-[440px] items-center gap-3">
              <Label
                className="text-grey-normal text-[16px] font-normal"
                htmlFor="email"
              >
                Email
              </Label>
              <Input
                type="email"
                id="email"
                placeholder="Enter Your Email"
                className="placeholder:text-primary-medium p-[16px] text-[15px] bg-white border-primary-ultralight h-[50px] rounded-[9px]"
              />
            </div>
          </div>
          {/* end col 3 */}
          <div className="lg:basis-1/2 mb-[15px] lg:mb-[30px]">
            <div className="grid w-full max-w-[440px] items-center gap-3">
              <Label
                className="text-grey-normal text-[16px] font-normal"
                htmlFor="number"
              >
                Number
              </Label>
              <Input
                type="number"
                id="number"
                placeholder="+966     Mobile Number"
                className="placeholder:text-primary-medium p-[16px] text-[15px] bg-white border-primary-ultralight h-[50px] rounded-[9px]"
              />
            </div>
          </div>
          {/* end col 4 */}

          <div className="lg:basis-1/2 mb-[15px] lg:mb-[30px]">
            <div className="grid w-full max-w-[440px] items-center gap-3">
              <Label
                className="text-grey-normal text-[16px] font-normal"
                htmlFor="nationality"
              >
                Nationality
              </Label>
              <Select>
                <SelectTrigger className="text-primary-medium p-[16px] text-[15px] bg-white border-primary-ultralight h-[50px] rounded-[9px] w-full">
                  <SelectValue placeholder="Choose Nationality" />
                </SelectTrigger>
                <SelectContent>
                  <SelectItem value="nepali">Nepali</SelectItem>
                  <SelectItem value="hindi">Hindi</SelectItem>
                  <SelectItem value="spanish">Spanish</SelectItem>
                </SelectContent>
              </Select>
            </div>
          </div>
          {/* end col 5 */}

          <div className="lg:basis-1/2 mb-[15px] lg:mb-[30px]">
            <div className="grid w-full max-w-[440px] items-center gap-3">
              <Label
                className="text-grey-normal text-[16px] font-normal"
                htmlFor="name"
              >
                City of Residence
              </Label>
              <Input
                type="name"
                id="name"
                placeholder="Enter your city of residence"
                className="placeholder:text-primary-medium p-[16px] text-[15px] bg-white border-primary-ultralight h-[50px] rounded-[9px]"
              />
            </div>
          </div>
          {/* end col 6 */}
          <div className="lg:basis-1/2 mb-[15px] lg:mb-[30px]">
            <div className="grid w-full max-w-[440px] items-center gap-3">
              <div className="flex items-center justify-between">
                <Label
                  className="text-grey-normal text-[16px] font-normal "
                  htmlFor="nationality"
                >
                  University
                </Label>
                <div className="flex items-center gap-1">
                  <OctagonAlert
                    size={24}
                    fill="#F0B441"
                    color="#fff"
                    strokeWidth={2}
                  />
                  <p className="text-[12px] text-grey-light tracking-[-0.01em]">
                    Choose you University from the drop down
                  </p>
                </div>
              </div>

              <Select>
                <SelectTrigger className="text-primary-medium p-[16px] text-[15px] bg-white border-primary-ultralight h-[50px] rounded-[9px] w-full">
                  <SelectValue placeholder="Select your University" />
                </SelectTrigger>
                <SelectContent>
                  <SelectItem value="tu">TU</SelectItem>
                  <SelectItem value="ku">KU</SelectItem>
                  <SelectItem value="pu">Pu</SelectItem>
                </SelectContent>
              </Select>
            </div>
          </div>
          {/* end col 7 */}
          <div className="lg:basis-1/2 mb-[15px] lg:mb-[30px]">
            <div className="grid w-full max-w-[440px] items-center gap-3">
              <div className="flex items-center justify-between">
                <Label
                  className="text-grey-normal text-[16px] font-normal"
                  htmlFor="nationality"
                >
                  Major
                </Label>
                <div className="flex items-center gap-1">
                  <OctagonAlert
                    size={24}
                    fill="#F0B441"
                    color="#fff"
                    strokeWidth={2}
                  />
                  <p className="text-[12px] text-grey-light tracking-[-0.01em]">
                    Choose you major from the drop down
                  </p>
                </div>
              </div>
              <Select>
                <SelectTrigger className="text-primary-medium p-[16px] text-[15px] bg-white border-primary-ultralight h-[50px] rounded-[9px] w-full">
                  <SelectValue placeholder="Select your Major" />
                </SelectTrigger>
                <SelectContent>
                  <SelectItem value="balen">Balen</SelectItem>
                  <SelectItem value="sunita">Sunita</SelectItem>
                  <SelectItem value="surendra">Surendra</SelectItem>
                </SelectContent>
              </Select>
            </div>
          </div>
          {/* end col 8 */}
          <div className="lg:basis-1/2 mb-[15px] lg:mb-[30px] lg:mt-[30px]">
            <p className="leading-none mb-[20px] text-grey-normal text-[16px]">
              Have You Completed University?
            </p>

            <RadioGroup defaultValue="Graduated">
              <div className="flex items-center space-x-2 mb-[20px]">
                <RadioGroupItem value="Graduated" id="Graduated" />
                <Label
                  className="text-primary-normal font-normal text-[15px]"
                  htmlFor="Graduated"
                >
                  Graduated from University
                </Label>
              </div>
              <div className="flex items-center space-x-2">
                <RadioGroupItem value="Currently" id="Currently" />
                <Label
                  className="text-primary-normal font-normal text-[15px]"
                  htmlFor="Currently"
                >
                  Currently at University
                </Label>
              </div>
            </RadioGroup>
          </div>
          {/* end col */}
          <div className="lg:basis-1/2 mb-[15px] lg:mb-[30px] lg:mt-[30px]">
            <p className="leading-none mb-[20px] text-grey-normal text-[16px]">
              If You are still at University, What Year Are You Currently In?
            </p>

            <RadioGroup defaultValue="less-year">
              <div className="flex items-center space-x-2 mb-[20px]">
                <RadioGroupItem value="less-year" id="less-year" />
                <Label
                  className="text-primary-normal font-normal text-[15px]"
                  htmlFor="less-year"
                >
                  0 – 1 Year
                </Label>
              </div>

              <div className="flex items-center space-x-2 mb-[20px]">
                <RadioGroupItem value="fouryear" id="fouryear" />
                <Label
                  className="text-primary-normal font-normal text-[15px]"
                  htmlFor="fouryear"
                >
                  2 – 3 Years
                </Label>
              </div>
              <div className="flex items-center space-x-2">
                <RadioGroupItem value="proficient" id="proficient" />
                <Label
                  className="text-primary-normal font-normal text-[15px]"
                  htmlFor="proficient"
                >
                  4+ Years
                </Label>
              </div>
            </RadioGroup>
          </div>
          {/* end col */}
        </div>
        {/* remove Member  */}
        <RemoveMember />
        {/* remove Member  */}
      </div>
      {/* team 2 */}
      <div className="mt-[20px] text-center">
        <p className="text-[16px] text-grey mb-[12px]">
          Add Member
        </p>
        <div className="h-[60px] w-[60px] rounded-[50%] m-auto bg-secondary hover:bg-secondary/80 leading-[60px] text-center cursor-pointer">
          <Plus
            size={24}
            fill="#A3B4F9"
            color="#0202AD"
            strokeWidth={1.5}
            className="inline"
          />
          {/* <Plus strokeWidth={0.5} /> */}
        </div>
      </div>
    </div>
  );
};

export default teamInfo;
