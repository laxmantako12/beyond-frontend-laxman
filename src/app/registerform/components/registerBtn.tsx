import React from "react";
// import { Button } from "@/components/ui/button";
import { Check } from "lucide-react";
import { CircleCheck } from "lucide-react";
import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
const registerBtn = () => {
  return (
    <div className="text-right mt-[30px] success-message">
      {/* <Button className="rounded-[3px] min-w-[120px] uppercase tracking-[.3em] text-[10px]">
        Register
      </Button> */}
      <Dialog>
        <DialogTrigger className="inline-flex items-center justify-center whitespace-nowrap ring-offset-background transition-colors focus-visible:outline-none focus-visible:ring-2 focus-visible:ring-ring focus-visible:ring-offset-2 disabled:pointer-events-none disabled:opacity-50 bg-primary text-white hover:bg-primary/80 h-[46px] px-[23px] font-bold rounded-[3px] min-w-[120px]  uppercase tracking-[.3em] text-[10px]">
          Register
        </DialogTrigger>
        <DialogContent className="bg-primary rounded-[20px] border-none p-[25px] lg:p-[50px] modal-success-message">
          <DialogHeader>
            <DialogTitle className="text-center">
              <div className="bg-white h-[90px] leading-[50px] m-auto w-[90px] rounded-[50%]  border-[18px] border-secondary">
                <Check
                  className="inline"
                  color="#F0B441"
                  size={30}
                  strokeWidth={3}
                />
              </div>
              <h4 className="text-white font-light text-[24px] leading-[24px] mb-[18px] mt-[24px]">
                Congratulations!{" "}
              </h4>
            </DialogTitle>
            <DialogDescription className="text-center">
              <p className="text-white font-light text-[14px] leading-[18px] mb-[20px] lg:mb-[40px]">
                Your registration for the competition has been successfully
                received, and we are thrilled to have you on board. Thank you
                for choosing to be a part of this exciting competition.
              </p>
              <p className="text-white font-light text-[14px] leading-[18px]">
                Your registration for the competition has been successfully
                received, and we are thrilled to have you on board. Thank you
                for choosing to be a part of this exciting competition.
              </p>
            </DialogDescription>
          </DialogHeader>
        </DialogContent>
      </Dialog>
    </div>
  );
};

export default registerBtn;
