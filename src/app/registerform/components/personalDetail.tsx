import React from "react";
import { Input } from "@/components/ui/input";
import { Label } from "@/components/ui/label";
import Datepicker from "./datepicker";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import { RadioGroup, RadioGroupItem } from "@/components/ui/radio-group";
import { OctagonAlert } from "lucide-react";

const personalDetail = () => {
  return (
    <div className="p-[25px] lg:p-[60px] mt-[12px] overflow-hidden bg-white rounded-[4px]">
      <h2 className="text-primary text-[14px] font-bold uppercase tracking-[.3em] mb-[20px] lg:mb-[40px]">
        personal Detail
      </h2>
      <div className="lg:flex lg:flex-wrap ">
        <div className="lg:basis-1/2 mb-[15px] lg:mb-[30px]">
          <div className="grid w-full max-w-[440px] items-center gap-3">
            <Label
              className="text-grey-normal text-[16px] font-normal"
              htmlFor="name"
            >
              Name
            </Label>
            <Input
              type="name"
              id="name"
              placeholder="Full Name"
              className="placeholder:text-primary-medium p-[16px] text-[15px] bg-white border-primary-ultralight h-[50px] rounded-[9px]"
            />
          </div>
        </div>
        {/* end col 1 */}

        <div className="lg:basis-1/2 mb-[15px] lg:mb-[30px]">
          <div className="grid w-full max-w-[440px] items-center gap-3">
            <Label
              className="text-grey-normal text-[16px] font-normal"
              htmlFor="number"
            >
              Number
            </Label>
            <Input
              type="number"
              id="number"
              placeholder="+966     Mobile Number"
              className="placeholder:text-primary-medium p-[16px] text-[15px] bg-white border-primary-ultralight h-[50px] rounded-[9px]"
            />
          </div>
        </div>

        {/* end col 2 */}

        <div className="lg:basis-1/2 mb-[15px] lg:mb-[30px]">
          <div className="grid w-full max-w-[440px] items-center gap-3">
            <Label
              className="text-grey-normal text-[16px] font-normal"
              htmlFor="email"
            >
              Email
            </Label>
            <Input
              type="email"
              id="email"
              placeholder="Enter Your Email"
              className="placeholder:text-primary-medium p-[16px] text-[15px] bg-white border-primary-ultralight h-[50px] rounded-[9px]"
            />
          </div>
        </div>
        {/* end col 3 */}
        {/* <div className="lg:basis-1/2 mb-[15px] lg:mb-[30px]">
          <div className="grid w-full max-w-[440px] items-center gap-3">
            <Label
              className="text-grey-normal text-[16px] font-normal"
              htmlFor="dob"
            >
              DOB
            </Label>
            <Datepicker />
          </div>
        </div> */}

        {/* end col 4 */}

        <div className="lg:basis-1/2 mb-[15px] lg:mb-[30px]">
          <div className="grid w-full max-w-[440px] items-center gap-3">
            <Label
              className="text-grey-normal text-[16px] font-normal"
              htmlFor="nationality"
            >
              Nationality
            </Label>
            <Select>
              <SelectTrigger className="text-primary-medium p-[16px] text-[15px] bg-white border-primary-ultralight h-[50px] rounded-[9px] w-full">
                <SelectValue placeholder="Choose Nationality" />
              </SelectTrigger>
              <SelectContent>
                <SelectItem value="nepali">Saudi </SelectItem>
                <SelectItem value="hindi">Non-Saudi </SelectItem>
              </SelectContent>
            </Select>
          </div>
        </div>
        {/* end col 5 */}

        <div className="lg:basis-1/2 mb-[15px] lg:mb-[30px]">
          <div className="grid w-full max-w-[440px] items-center gap-3">
            <Label
              className="text-grey-normal text-[16px] font-normal"
              htmlFor="Country"
            >
              Country of Residence
            </Label>
            <Select>
              <SelectTrigger className="text-primary-medium p-[16px] text-[15px] bg-white border-primary-ultralight h-[50px] rounded-[9px] w-full">
                <SelectValue placeholder="Choose Country" />
              </SelectTrigger>
              <SelectContent>
                <SelectItem value="UAE">UAE </SelectItem>
                <SelectItem value="Egypt">Egypt </SelectItem>
                <SelectItem value="Tunisia">Tunisia </SelectItem>
                <SelectItem value="Morocco">Morocco </SelectItem>
              </SelectContent>
            </Select>
          </div>
        </div>
        {/* end col 6 */}
        <div className="lg:basis-1/2 mb-[15px] lg:mb-[30px]">
          <div className="grid w-full max-w-[440px] items-center gap-3">
            <div className="flex items-center justify-between">
              <Label
                className="text-grey-normal text-[16px] font-normal "
                htmlFor="University"
              >
                University
              </Label>
              <div className="flex items-center gap-1">
                <OctagonAlert
                  size={24}
                  fill="#F0B441"
                  color="#fff"
                  strokeWidth={2}
                />
                <p className="text-[12px] text-grey-light tracking-[-0.01em]">
                  Choose you University from the drop down
                </p>
              </div>
            </div>

            <Select>
              <SelectTrigger className="text-primary-medium p-[16px] text-[15px] bg-white border-primary-ultralight h-[50px] rounded-[9px] w-full">
                <SelectValue placeholder="Select your University" />
              </SelectTrigger>
              <SelectContent>
                <SelectItem value="tu">TU</SelectItem>
                <SelectItem value="ku">KU</SelectItem>
                <SelectItem value="pu">Pu</SelectItem>
              </SelectContent>
            </Select>
          </div>
        </div>

        <div className="lg:basis-1/2 mb-[15px] lg:mb-[30px]">
          <div className="grid w-full max-w-[440px] items-center gap-3">
            <Label
              className="text-grey-normal text-[16px] font-normal"
              htmlFor="City"
            >
              City of Residence
            </Label>
            <Select>
              <SelectTrigger className="text-primary-medium p-[16px] text-[15px] bg-white border-primary-ultralight h-[50px] rounded-[9px] w-full">
                <SelectValue placeholder="Choose City" />
              </SelectTrigger>
              <SelectContent>
                <SelectItem value="Riyadh">Riyadh </SelectItem>
                <SelectItem value="Jeddah">Jeddah</SelectItem>
                <SelectItem value="Makkah">Makkah</SelectItem>
                <SelectItem value="Madina">Madina </SelectItem>
                <SelectItem value="AlKhobar">Al Khobar</SelectItem>
                <SelectItem value="Taif">Taif</SelectItem>
                <SelectItem value="Other">Other</SelectItem>
              </SelectContent>
            </Select>
          </div>
        </div>

        {/* end col 7 */}
        <div className="lg:basis-1/2 mb-[15px] lg:mb-[30px]">
          <div className="grid w-full max-w-[440px] items-center gap-3">
            <Label
              className="text-grey-normal text-[16px] font-normal"
              htmlFor="Gender"
            >
              Gender
            </Label>
            <Select>
              <SelectTrigger className="text-primary-medium p-[16px] text-[15px] bg-white border-primary-ultralight h-[50px] rounded-[9px] w-full">
                <SelectValue placeholder="Choose Gender" />
              </SelectTrigger>
              <SelectContent>
                <SelectItem value="nepali">Male </SelectItem>
                <SelectItem value="hindi">Female</SelectItem>
              </SelectContent>
            </Select>
          </div>
        </div>
        {/* end col 7 */}
        <div className="lg:basis-1/2 mb-[15px] lg:mb-[30px]">
          <div className="grid w-full max-w-[440px] items-center gap-3">
            <div className="flex items-center justify-between">
              <Label
                className="text-grey-normal text-[16px] font-normal"
                htmlFor="Age"
              >
                Age
              </Label>
              <div className="flex items-center gap-1">
                <OctagonAlert
                  size={24}
                  fill="#F0B441"
                  color="#fff"
                  strokeWidth={2}
                />
                <p className="text-[12px] text-grey-light tracking-[-0.01em]">
                  Choose you age from the drop down
                </p>
              </div>
            </div>
            <Select>
              <SelectTrigger className="text-primary-medium p-[16px] text-[15px] bg-white border-primary-ultralight h-[50px] rounded-[9px] w-full">
                <SelectValue placeholder="Select your Age" />
              </SelectTrigger>
              <SelectContent>
                <SelectItem value="1">Less than 18</SelectItem>
                <SelectItem value="2">18 – 20</SelectItem>
                <SelectItem value="3">21 – 25</SelectItem>
                <SelectItem value="4">26 - 30</SelectItem>
                <SelectItem value="5">31 - 35</SelectItem>
                <SelectItem value="6">36 – 40</SelectItem>
                <SelectItem value="7">40+</SelectItem>
              </SelectContent>
            </Select>
          </div>
        </div>
        {/* end col 8 */}

        <div className="lg:basis-1/2 mb-[15px] lg:mb-[30px]">
          <div className="grid w-full max-w-[440px] items-center gap-3">
            <div className="flex items-center justify-between">
              <Label
                className="text-grey-normal text-[16px] font-normal"
                htmlFor="educational_qualification "
              >
                Educational Qualification
              </Label>
            </div>
            <Select>
              <SelectTrigger className="text-primary-medium p-[16px] text-[15px] bg-white border-primary-ultralight h-[50px] rounded-[9px] w-full">
                <SelectValue placeholder="Select your Educational Qualification  " />
              </SelectTrigger>
              <SelectContent>
                <SelectItem value="1">High School</SelectItem>
                <SelectItem value="2">Diploma</SelectItem>
                <SelectItem value="3">Bachelors </SelectItem>
                <SelectItem value="4">Masters </SelectItem>
                <SelectItem value="5">PhD</SelectItem>
                <SelectItem value="6">Other </SelectItem>
              </SelectContent>
            </Select>
          </div>
        </div>
        {/* end  */}
        <div className="lg:basis-1/2 mb-[15px] lg:mb-[30px]">
          <div className="grid w-full max-w-[440px] items-center gap-3">
            <Label
              className="text-grey-normal text-[16px] font-normal"
              htmlFor="Educational_Institution_name"
            >
              Educational Institution Name
            </Label>
            <Input
              type="name"
              id="name"
              placeholder="Educational Institution Name"
              className="placeholder:text-primary-medium p-[16px] text-[15px] bg-white border-primary-ultralight h-[50px] rounded-[9px]"
            />
          </div>
        </div>
        {/* end col  */}
        <div className="lg:basis-1/2 mb-[15px] lg:mb-[30px]">
          <div className="grid w-full max-w-[440px] items-center gap-3">
            <div className="flex items-center justify-between">
              <Label
                className="text-grey-normal text-[16px] font-normal"
                htmlFor="Occupation"
              >
                Occupation
              </Label>
            </div>
            <Select>
              <SelectTrigger className="text-primary-medium p-[16px] text-[15px] bg-white border-primary-ultralight h-[50px] rounded-[9px] w-full">
                <SelectValue placeholder="Select your Occupation" />
              </SelectTrigger>
              <SelectContent>
                <SelectItem value="1">University Student </SelectItem>
                <SelectItem value="2">Entrepreneur</SelectItem>
                <SelectItem value="3">Technology Expert </SelectItem>
                <SelectItem value="4">
                  MastCinematic Technology Experters{" "}
                </SelectItem>
                <SelectItem value="6">Other </SelectItem>
              </SelectContent>
            </Select>
          </div>
        </div>
        {/* end col  */}
        <div className="lg:basis-1/2 mb-[15px] lg:mb-[30px]">
          <div className="grid w-full max-w-[440px] items-center gap-3">
            <div className="flex items-center justify-between">
              <Label
                className="text-grey-normal text-[16px] font-normal"
                htmlFor="Experience"
              >
                Number of Years in Technical Experience
              </Label>
            </div>
            <Select>
              <SelectTrigger className="text-primary-medium p-[16px] text-[15px] bg-white border-primary-ultralight h-[50px] rounded-[9px] w-full">
                <SelectValue placeholder="Select your Experience" />
              </SelectTrigger>
              <SelectContent>
                <SelectItem value="1">Less than 0</SelectItem>
                <SelectItem value="2">1</SelectItem>
                <SelectItem value="3">2</SelectItem>
                <SelectItem value="4">3</SelectItem>
                <SelectItem value="5">4</SelectItem>
                <SelectItem value="6">5</SelectItem>
                <SelectItem value="7">6+</SelectItem>
              </SelectContent>
            </Select>
          </div>
        </div>
        {/* end col  */}
        <div className="lg:basis-1/2 mb-[15px] lg:mb-[30px]">
          <div className="grid w-full max-w-[440px] items-center gap-3">
            <div className="flex items-center justify-between">
              <Label
                className="text-grey-normal text-[16px] font-normal"
                htmlFor="Expertise "
              >
                Area of Expertise{" "}
              </Label>
              <div className="flex items-center gap-1">
                <OctagonAlert
                  size={24}
                  fill="#F0B441"
                  color="#fff"
                  strokeWidth={2}
                />
                <p className="text-[12px] text-grey-light tracking-[-0.01em]">
                  Participant can choose more than one option
                </p>
              </div>
            </div>
            <Select>
              <SelectTrigger className="text-primary-medium p-[16px] text-[15px] bg-white border-primary-ultralight h-[50px] rounded-[9px] w-full">
                <SelectValue placeholder="Select Area of Expertise  " />
              </SelectTrigger>
              <SelectContent>
                <SelectItem value="0">Artificial intelligence (Ai)</SelectItem>
                <SelectItem value="1">Robotics </SelectItem>
                <SelectItem value="2">Blockchain </SelectItem>
                <SelectItem value="3">Data Analysis </SelectItem>
                <SelectItem value="4">Cyber security</SelectItem>
                <SelectItem value="5">Virtual Reality</SelectItem>
              </SelectContent>
            </Select>
          </div>
        </div>
        {/* end col  */}
        {/* <div className="lg:basis-1/2 mb-[15px] lg:mb-[30px] lg:mt-[30px]">
          <p className="leading-none mb-[20px] text-grey-normal text-[16px]">
            Have You Completed University?
          </p>

          <RadioGroup defaultValue="Graduated">
            <div className="flex items-center space-x-2 mb-[20px]">
              <RadioGroupItem value="Graduated" id="Graduated" />
              <Label
                className="text-primary-normal font-normal text-[15px]"
                htmlFor="Graduated"
              >
                Graduated from University
              </Label>
            </div>
            <div className="flex items-center space-x-2">
              <RadioGroupItem value="Currently" id="Currently" />
              <Label
                className="text-primary-normal font-normal text-[15px]"
                htmlFor="Currently"
              >
                Currently at University
              </Label>
            </div>
          </RadioGroup>
        </div> */}
        {/* end col */}
        {/* <div className="lg:basis-1/2 mb-[15px] lg:mb-[30px] lg:mt-[30px]">
          <p className="leading-none mb-[20px] text-grey-normal text-[16px]">
            If You are still at University, What Year Are You Currently In?
          </p>

          <RadioGroup defaultValue="less-year">
            <div className="flex items-center space-x-2 mb-[20px]">
              <RadioGroupItem value="less-year" id="less-year" />
              <Label
                className="text-primary-normal font-normal text-[15px]"
                htmlFor="less-year"
              >
                0 – 1 Year
              </Label>
            </div>

            <div className="flex items-center space-x-2 mb-[20px]">
              <RadioGroupItem value="fouryear" id="fouryear" />
              <Label
                className="text-primary-normal font-normal text-[15px]"
                htmlFor="fouryear"
              >
                2 – 3 Years
              </Label>
            </div>
            <div className="flex items-center space-x-2">
              <RadioGroupItem value="proficient" id="proficient" />
              <Label
                className="text-primary-normal font-normal text-[15px]"
                htmlFor="proficient"
              >
                4+ Years
              </Label>
            </div>
          </RadioGroup>
        </div> */}
        {/* end col */}
      </div>
    </div>
  );
};

export default personalDetail;
