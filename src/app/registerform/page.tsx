import React from "react";
import PersonalDetail from "./components/personalDetail";
import Enrollment from "./components/enrollment";
import TeamInfo from "./components/teamInfo";
import TechnicalInfo from "./components/technicalInfo";
import ReasonForRegister from "./components/reasonForRegister";
import Registerbtn from "./components/registerBtn";
import UploadFile from "./components/uploadfile";


const registerForm = () => {
  return (
    <>
      <div className="formRegister pb-[30px] lg:pb-[60px]">
        <div className="container">
          <h1 className="my-5 lg:mt-8 lg:mb-10 text-primary font-bold uppercase tracking-[.3em]">
            Registration form
          </h1>
          <PersonalDetail />
          <Enrollment />
          <TeamInfo />
          <TechnicalInfo />
          <ReasonForRegister />
          <UploadFile />
          <Registerbtn />
        </div>
      </div>
    </>
  );
};

export default registerForm;
