import React from 'react';
// import Navigation from './components/navigation';
import Banner from './components/banner';
import About from './components/about';
import Leaders from './components/leaders';
import FilmationTracks from './components/filmation-tracks';
import SignIn from './components/sign-in';
// import Footer from './components/footer';

const ParticipantPage = () => {
  return (
    <div>
      {/* <Navigation /> */}
      <Banner />
      <About />
      <Leaders />
      <FilmationTracks />
      <SignIn />
      {/* <Footer /> */}
    </div>
  );
};

export default ParticipantPage;
