'use client';

import React, { useEffect, useState } from 'react';
import moment from 'moment';

const targetTime = moment('2035-01-01');
const CountdownTime = () => {
  const [currentTime, setCurrentTime] = useState(moment());
  const timeBetween = moment.duration(targetTime.diff(currentTime));

  useEffect(() => {
    const interval = setInterval(() => {
      setCurrentTime(moment());
    }, 1000);

    return () => clearInterval(interval);
  }, []);
  return (
    <div>
      <>
        <div className='counter flex mb-[15px] mt-[5px] lg:mb-[30px] lg:mt-[30px]'>
          {/* <span>{timeBetween.years()}yr </span>
        <span>{timeBetween.months()}m </span> */}
          <div className='item text-center text-primary-normal pr-[10px] lg:pr-[25px] lg:text-[42px]'>
            {timeBetween.days()}
            <span className='font-bold block text-secondary text-[8px] tracking-[0.20rem] uppercase mt-[10px] lg:mt-[20px]'>
              days{' '}
            </span>
          </div>
          <div className='item text-center text-primary-normal pr-[10px] lg:pr-[25px] lg:text-[42px]'>
            {timeBetween.hours()}
            <span className='font-bold block text-secondary text-[8px] tracking-[0.20rem] uppercase mt-[10px] lg:mt-[20px]'>
              hours{' '}
            </span>
          </div>
          <div className='item text-center text-primary-normal pr-[10px] lg:pr-[25px] lg:text-[42px]'>
            {timeBetween.minutes()}
            <span className='font-bold block text-secondary text-[8px] tracking-[0.20rem] uppercase mt-[10px] lg:mt-[20px]'>
              minute{' '}
            </span>
          </div>
          <div className='item text-center text-primary-normal pr-[10px] lg:pr-[25px] lg:text-[42px]'>
            {timeBetween.seconds()}
            <span className='font-bold block text-secondary text-[8px] tracking-[0.20rem] uppercase mt-[10px] lg:mt-[20px]'>
              second{' '}
            </span>
          </div>
        </div>
      </>
    </div>
  );
};

export default CountdownTime;
