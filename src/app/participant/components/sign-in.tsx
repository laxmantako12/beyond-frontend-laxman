"use client";
import React from "react";
import { Input } from "@/components/ui/input";
import { Checkbox } from "@/components/ui/checkbox";
import { Button } from "@/components/ui/button";
import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import { z } from "zod";
import {
  Form,
  FormControl,
  FormDescription,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";

function SignIn() {
  const formSchema = z.object({
    email: z.string().email({
      message: "Please enter a valid email address.",
    }),
    password: z.string().min(6, {
      message: "Password must be at least 6 characters long.",
    }),
  });

  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      email: "",
      password: "",
    },
  });

  function onSubmit(values: z.infer<typeof formSchema>) {
    // Do something with the form values.
    console.log(values);
  }

  return (
    <div className="signIn pb-[80px] pt-[80px] bg-white">
      <div className="container">
        <div className="lg:flex -ml-2.5 -mr-2.5">
          <div className="lg:basis-1/2 pl-2.5 pr-2.5">
            <div className="max-w-[595px] w-full">
              <h2 className="uppercase mb-[25px] font-medium text-primary-normal text-[14px] tracking-[0.3em]">
                Sign in to your account
              </h2>
              <p className="text-[16px] text-accent">
                Lorem ipsum dolor sit amet consectetur adipiscing eli mattis sit
                aliquam sit nullam. Lorem ipsum dolor sit amet consectetur
                adipiscing.
              </p>
            </div>
          </div>
          <div className="lg:basis-1/2 pl-2.5 pr-2.5">
            <div className="max-w-[540px] w-full ml-auto mr-0">
              <Form {...form}>
                <form
                  onSubmit={form.handleSubmit(onSubmit)}
                  className="space-y-8"
                >
                  <FormField
                    control={form.control}
                    name="email"
                    render={({ field }) => (
                      <FormItem>
                        <FormLabel className="hidden">Email</FormLabel>
                        <FormControl>
                          <Input
                            className="rounded-none p-0 border-t-0 border-r-0 border-l-0 border-b-1 bg-transparent outline-none shadow-none"
                            type="email"
                            placeholder="Email address"
                            {...field}
                          />
                        </FormControl>
                        <FormDescription>
                          {/* Enter your email address. */}
                        </FormDescription>
                        <FormMessage />
                      </FormItem>
                    )}
                  />
                  <FormField
                    control={form.control}
                    name="password"
                    render={({ field }) => (
                      <FormItem>
                        <FormLabel className="hidden">Password</FormLabel>
                        <FormControl>
                          <Input
                            className="rounded-none p-0 border-t-0 border-r-0 border-l-0 border-b-1 bg-transparent outline-none"
                            type="password"
                            placeholder="Password"
                            {...field}
                          />
                        </FormControl>
                        <FormDescription>
                          {/* Enter your password. */}
                        </FormDescription>
                        <FormMessage />
                      </FormItem>
                    )}
                  />
                  <div className="space-x-2 flex items-center">
                    <Checkbox className="mr-1 h-[15px] w-[15px] border-gray rounded-sm" id="terms" />
                    <label
                      htmlFor="terms"
                      className="text-[10px] font-normal uppercase tracking-[0.25em]"
                    >
                      Remember me
                    </label>
                    
                  </div>
                  <div className="space-x-2">
                  <p className="text-muted-foreground text-[10px] text-garkgray">
                      This portal is dedicated to the eligible participants in
                      the Filmathon initiative.
                    </p>
                  </div>
                  <Button
                    type="submit"
                    className="uppercase font-normal text-xs/[10px] tracking-[0.3em]"
                    variant="outline">
                    Sign in
                  </Button>
                </form>
              </Form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default SignIn;
