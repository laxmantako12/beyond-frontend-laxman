import React from "react";

function About() {
  return (
    <div className="about pt-[80px] pb-[80px]">
      <div className="container text-center ">
        <div className="ml-auto mr-auto max-w-[600px]">
          <h2 className="uppercase mb-6 font-medium text-primary text-xs/[10px] tracking-[0.3em]">
            About the Filmathon
          </h2>
          <p className="leading-[24px] text-accent">
            Lorem ipsum dolor sit amet consectetur adipiscing eli mattis sit
            aliquam sit nullam. Lorem ipsum dolor sit amet consectetur
            adipiscing eli mattis sit aliquam sit nullam ipsum dolor sit amet
            consectetur.
          </p>
        </div>
      </div>
    </div>
  );
}

export default About;
