import React from "react";
import Image from "next/image";
function FilmationTracks() {
  const FilmationTracks = [
    {
      src: "/images/tracks/track1.png",
      alt: "track 1",
      title: "lorem ipsum dolor sit amet alequea.",
      content:
        "Lorem ipsum dolor sit amet consectetur adipiscing eli mattis sit phasellus mollis si, lorem ipsum.",
    },
    {
      src: "/images/tracks/track2.png",
      alt: "track 2",
      title: "lorem ipsum dolor sit amet alequea.",
      //   content: "track 2 Caption",
    },
  ];

  return (
    <div className="FilmationTracks pb-[80px] pt-[80px] bg-gray" id="tracks">
      <div className="container">
        <h2 className="uppercase mb-[34px] font-medium text-center text-white text-xs/[10px] tracking-[0.3em]">
          Filmathon Tracks
        </h2>

        <div className="lg:flex -ml-1.5 -mr-1.5">
          {FilmationTracks.map((FilmationTrack, index) => (
            <div key={index} className="lg:basis-1/2 pl-1.5 pr-1.5">
              <div className="rounded-[16px] overflow-hidden mb-5 lg:mb-8">
                <div className="rounded-[16px] border-0">
                  <div className="flex p-0 items-center justify-center relative">
                    <figure className="w-full relative pb-[56.25%] ">
                      <Image
                        src={FilmationTrack.src}
                        alt={FilmationTrack.alt}
                        layout="fill"
                        // width={FilmationTrack.width}
                        // height={FilmationTrack.height}
                        className="object-cover"
                      />
                      <figcaption className="text-sm p-[30px] absolute w-full bottom-0 text-white">
                        <h3 className="mb-[13px] uppercase font-semibold text-sm tracking-[0.2em]">
                          {FilmationTrack.title}
                        </h3>
                        <p className="text-[17px]">{FilmationTrack.content}</p>
                      </figcaption>
                    </figure>
                  </div>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}

export default FilmationTracks;
