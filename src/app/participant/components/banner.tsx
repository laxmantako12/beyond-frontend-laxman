import React from 'react';
import { Button } from '@/components/ui/button';
import CountdownTime from './countdown-time';
import Link from "next/link";

const bgImg = {
  // backgroundColor: '#69C2B8',
  backgroundImage: 'url(../images/banner.jpg)',
  backgroundSize: 'cover',
  backgroundPosition: 'center',
  backgroundRepeat: 'no-repeat',
  minHeight: '465px',
};

function Banner() {
  return (
    <div className="mainBanner flex items-center pt-10">
      <div className="container">
        <div
          className="mainBanner__wrapper flex items-center overflow-hidden rounded-[18px] p-[20px] lg:p-[50px]"
          style={bgImg}
        >
          <div className="mainBanner__caption flex flex-wrap flex-col justify-center max-w-[580px] w-full p-[20px] lg:p-[50px] ml-auto mr-0 bg-secondary-ultralight/95  overflow-hidden rounded-[18px] min-h-[350px]">
            <h1 className="text-secondary text-[24px] lg:text-[40px] uppercase tracking-[0.4rem] mb-[15px] lg:mb-[20px] font-normal">
              Filmathon
            </h1>
            <span className="text-grey-normal text-[14px] lg:text-[18px] uppercase tracking-[0.2rem]  mb-[15px] lg:mb-[20px] font-medium">
              Innovation in Filmmaking
            </span>
            <div className="timer">
              <CountdownTime />
            </div>
            <div className="lg:flex items-center">
              <div className="one ">
                <Link href="/registerform">
                  <Button
                    className="uppercase text-primary-normal font-normal text-xs/[10px] tracking-[0.3em]"
                    variant="outline"
                  >
                    Register
                  </Button>
                </Link>
              </div>
              <div className="text-grey-normal text-[10px] uppercase tracking-[0.2rem] lg:ml-[25px] lg:mt-[0] mt-[15px] ">
                (Hybrid) Virtual & In-person
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Banner;
