import React from "react";

const winnersData = [
  {
    class: "title-block title-block-1",
    name: "John Doe",
    role: "(Team Leader)",
    nationality: "UAE",
    nationalitytitle: "Nationality",
    membertitle: "Members",
    members: ["Antony", "Steven"],
  },
  {
    class: "title-block title-block-2",
    name: "Alok",
    //role: "(Team Leader)",
    nationality: "USA",
    nationalitytitle: "Nationality",
    //: "Members",
    // members: ["Michael", "Emily"]
    members: [],
  },
  {
    class: "title-block title-block-3",
    name: "MAXWELL",
    //role: "(Team Leader)",
    nationality: "USA",
    nationalitytitle: "Nationality",
    //membertitle: "Members",
    // members: ["Michael", "Emily"]
    members: [],
  },
  // Add more winners as needed
];

const FirstWinner = () => {
  return (
    <div className="mb-6">
      <div className="container">
        <h2 className="text-primary-medium text-[10px] font-bold uppercase tracking-[.3em]">
          1st Winners
        </h2>
        <div className="lg:flex -mx-2">
          {winnersData.map((winner, index) => (
            <div key={index} className="basis-1/3 px-2 my-4">
              <div className="bg-primary p-4 rounded-[15px] text-white lg:min-h-[137px]">
                <div className={`min-h-[33px] ${winner.class}`}>
                  <h3 className="text-[12px] font-bold uppercase tracking-[.3em] mb-0.5">
                    {winner.name}
                  </h3>
                  <span className="block text-[11px] font-normal leading-none">
                    {winner.role}
                  </span>
                </div>
                <div className="flex">
                  <div className="basis-1/2 lg:basis-1/3 mt-2.5">
                    <h4 className="text-[11px] font-medium mb-0.5">
                      {winner.nationalitytitle}
                    </h4>
                    <span className="block text-[11px] font-normal leading-none">
                      {winner.nationality}
                    </span>
                  </div>
                  <div className="basis-1/2 lg:basis-1/3 mt-2.5">
                    <h4 className="text-[11px] font-medium mb-0.5">
                      {winner.membertitle}
                    </h4>
                    <ol>
                      {winner.members.map((member, index) => (
                        <li
                          key={index}
                          className="list-decimal list-inside text-[11px] font-light"
                        >
                          {member}
                        </li>
                      ))}
                    </ol>
                  </div>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default FirstWinner;
