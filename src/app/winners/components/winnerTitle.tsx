import React from 'react'

const winnerTitle = () => {
  return (
    <div className='container'>
        <h1 className='text-primary text-[14px] my-6 lg:mt-8 lg:mb-[40px] font-bold uppercase tracking-[.3em]'>Winners</h1>
    </div>
  )
}

export default winnerTitle