import React from 'react'
import WinnerTitle from './components/winnerTitle'
import Firstwinner from './components/firstWinner'
import Secondwinner from './components/secondWinner'
import Thirdwinner from './components/thirdWinner'

const winnersPage = () => {
  return (
    <div className="winners pb-[30px] lg:pb-[55px]">
        <WinnerTitle/>
        <Firstwinner/>
        <Secondwinner/>
        <Thirdwinner/>
    </div>
  )
}

export default winnersPage