'use client';
import React, { useState } from "react";
import {
  Accordion,
  AccordionContent,
  AccordionItem,
  AccordionTrigger,
} from "@/components/ui/accordion";
import { Button } from "@/components/ui/button";
import { Input } from "@/components/ui/input";
import { Loader } from "lucide-react";

const accordionData = [
  {
    title: "How to use the filmathon platform?",
    content:
      "Lorem ipsum dolor sit amet consectetur adipiscing eli mattis sit aliquam sit nullam. Lorem ipsum dolor sit amet consectetur adipiscing eli mattis sit aliquam sit nullam ipsum dolor sit amet consectetur.",
  },
  {
    title: "Registration Phase Evaluation: Phase 1",
    content:
      "Lorem ipsum dolor sit amet consectetur adipiscing eli mattis sit aliquam sit nullam. Lorem ipsum dolor sit amet consectetur adipiscing eli mattis sit aliquam sit nullam ipsum dolor sit amet consectetur.",
  },
  {
    title: "Judging Phase Evaluation: Phase 4",
    content:
      "Lorem ipsum dolor sit amet consectetur adipiscing eli mattis sit aliquam sit nullam. Lorem ipsum dolor sit amet consectetur adipiscing eli mattis sit aliquam sit nullam ipsum dolor sit amet consectetur.",
  },
  {
    title: "Judging Phase Evaluation: Phase 5",
    content:
      "Lorem ipsum dolor sit amet consectetur adipiscing eli mattis sit aliquam sit nullam. Lorem ipsum dolor sit amet consectetur adipiscing eli mattis sit aliquam sit nullam ipsum dolor sit amet consectetur.",
  },
  {
    title: "Public Voting",
    content:
      "Lorem ipsum dolor sit amet consectetur adipiscing eli mattis sit aliquam sit nullam. Lorem ipsum dolor sit amet consectetur adipiscing eli mattis sit aliquam sit nullam ipsum dolor sit amet consectetur.",
  },
  {
    title: "Announce Winners",
    content:
      "Lorem ipsum dolor sit amet consectetur adipiscing eli mattis sit aliquam sit nullam. Lorem ipsum dolor sit amet consectetur adipiscing eli mattis sit aliquam sit nullam ipsum dolor sit amet consectetur.",
  },
  {
    title: "Announce Winners",
    content:
      "Lorem ipsum dolor sit amet consectetur adipiscing eli mattis sit aliquam sit nullam. Lorem ipsum dolor sit amet consectetur adipiscing eli mattis sit aliquam sit nullam ipsum dolor sit amet consectetur.",
  },
  {
    title: "Announce Winners",
    content:
      "Lorem ipsum dolor sit amet consectetur adipiscing eli mattis sit aliquam sit nullam. Lorem ipsum dolor sit amet consectetur adipiscing eli mattis sit aliquam sit nullam ipsum dolor sit amet consectetur.",
  },
  {
    title: "Announce Winners",
    content:
      "Lorem ipsum dolor sit amet consectetur adipiscing eli mattis sit aliquam sit nullam. Lorem ipsum dolor sit amet consectetur adipiscing eli mattis sit aliquam sit nullam ipsum dolor sit amet consectetur.",
  },
];

const FaqPage = () => {
  const [openItem, setOpenItem] = useState("0");

  return (
    <div className="FaqPage  py-14">
      <div className="container">
        <div className="search-block text-center">
          <h1 className="text-primary font-bold mb-[18px] uppercase tracking-[.3em]">
            Frequently Asked Questions
          </h1>
          <p className="text-grey-normal text-[16px] ">
            Have a question? We are here to help!
          </p>
          <div className="flex w-full max-w-sm m-auto items-center space-x-2 mt-5 lg:mt-[25px]">
            <Input
              type="text"
              placeholder="Search"
              className="bg-white border-primary-ultralight/40 h-[40px] rounded-[9px] placeholder:font-normal placeholder:primary-ultralight/40 tracking-[0.075em]"
            />
            <Button
              type="submit"
              className="bg-secondary tracking-[.3em] text-[10px] font-normal rounded-[9px] h-[40px]"
            >
              SEARCH
            </Button>
          </div>
        </div>
        <div className="mt-5 lg:mt-[45px] lg:mb-10">
          <div className="rounded-2xl overflow-hidden shadow-container">
            <Accordion
              type="single"
              collapsible
              className="w-full"
              defaultValue="0"
              onValueChange={(value) => setOpenItem(value)}
            >
              {accordionData.map((item, index) => (
                <AccordionItem
                  key={index}
                  value={String(index)}
                  className="first:rounded-t-2xl last:rounded-b-2xl p-0 border-primary-ultralight/40 bg-secondary-light"
                >
                  <AccordionTrigger
                    className={`px-7 py-5 font-medium bg-white text-[15px] text-primary-normal ${
                      openItem === String(index)
                        ? "bg-secondary-light pb-0 " // Apply background color when open
                        : ""
                    }`}
                  >
                    <div className="flex justify-start items-center text-left w-full">
                      {item.title}
                    </div>
                  </AccordionTrigger>
                  <AccordionContent className="px-7 py-4 bg-secondary-light">
                    <div className="text-grey-normal text-[16px] ">
                      {item.content}
                    </div>
                  </AccordionContent>
                </AccordionItem>
              ))}
            </Accordion>
          </div>
        </div>
        <div className="loader text-center">
          <Loader
            strokeWidth={1}
            absoluteStrokeWidth
            // className="inline animate-spin duration-1000 delay-1000"
            className="inline duration-1000 delay-1000"
          />
        </div>
      </div>
    </div>
  );
};

export default FaqPage;
