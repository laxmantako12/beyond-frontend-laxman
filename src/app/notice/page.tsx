import React from 'react';
import Title from './components/title';
import Timeline from './components/timeline';
const NoticeBoard = () => {
  return (
    <div className='notice py-14'>
      <div className='container'>
        <Title />
        <Timeline />
      </div>
    </div>
  );
};

export default NoticeBoard;
