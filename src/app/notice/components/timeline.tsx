'use client';

import {
  VerticalTimeline,
  VerticalTimelineElement,
} from 'react-vertical-timeline-component';
import 'react-vertical-timeline-component/style.min.css';
import { timelineData } from './data';

const Timeline = () => {
  return (
    <div
      className={`timeline py-12 mt-6 overflow-hidden bg-white rounded-[15px] shadow-lg`}
    >
      <VerticalTimeline>
        {timelineData.map((timeline) => (
          <VerticalTimelineElement
            key={timeline.icon}
            iconClassName={timeline.iconClassName}
            icon={timeline.icon}
            textClassName={timeline.titleClassname}
          >
            <div className='-mt-2'>
              {timeline.title && (
                <h3 className='vertical-timeline-element-title font-medium text-[15px]'>
                  {timeline.title}
                </h3>
              )}

              {timeline.description && (
                <p className='font-light text-[13px] text-grey-normal m-0'>
                  {timeline.description}
                </p>
              )}
            </div>
          </VerticalTimelineElement>
        ))}
      </VerticalTimeline>
    </div>
  );
};

export default Timeline;
