import React from 'react'

const title = () => {
  return (
    <div className="text-center">
      <h1 className="text-primary font-bold mb-[18px] uppercase tracking-[.3em]">
        notice board
      </h1>
      <p className="text-grey-normal text-[16px] ">
        Have a question? We are here to help!
      </p>
    </div>
  );
}

export default title
