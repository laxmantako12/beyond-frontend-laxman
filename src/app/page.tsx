'use client';

import { SelectOption } from '@/components/search-input';
import {
  Accordion,
  AccordionContent,
  AccordionItem,
  AccordionTrigger,
} from '@/components/ui/accordion';
import ParticipantPage from "./participant/page"

const optionsData: SelectOption[] = [
  {
    id: '9cb0e66a-9937-465d-a188-2c4c4ae2401f',
    name: 'Grammatical Standard English',
  },
  {
    id: '61eb0e32-2391-4cd3-adc3-66efe09bc0b7',
    name: 'Summarize for a 2nd grader',
  },
  {
    id: 'a4e1fa51-f4ce-4e45-892c-224030a00bdd',
    name: 'Text to command',
  },
  {
    id: 'cc198b13-4933-43aa-977e-dcd95fa30770',
    name: 'Q&A',
  },
];

export default function Home() {
  return (
    <>
    <ParticipantPage />
      <main className='max-w-96 m-auto mt-5 hidden'>
        <div className='rounded-2xl'>
          <Accordion type='single' collapsible className='w-full'>
            {['1', '2', '3', '4'].map((item) => (
              <AccordionItem
                key={item}
                value={item}
                className='first:rounded-t-2xl last:rounded-b-2xl'
              >
                <AccordionTrigger className=''>
                  Is it accessible?
                </AccordionTrigger>
                <AccordionContent className=''>
                  Yes. It adheres to the WAI-ARIA design pattern.
                </AccordionContent>
              </AccordionItem>
            ))}
            {/* <AccordionItem
            value='item-1'
            className='first:rounded-t-2xl last:rounded-b-2xl'
          >
            <AccordionTrigger className=''>Is it accessible?</AccordionTrigger>
            <AccordionContent className=''>
              Yes. It adheres to the WAI-ARIA design pattern.
            </AccordionContent>
          </AccordionItem>
          <AccordionItem value='item-2'>
            <AccordionTrigger>Is it accessible?</AccordionTrigger>
            <AccordionContent>
              Yes. It adheres to the WAI-ARIA design pattern.
            </AccordionContent>
          </AccordionItem>
          <AccordionItem value='item-3'>
            <AccordionTrigger>Is it accessible?</AccordionTrigger>
            <AccordionContent>
              Yes. It adheres to the WAI-ARIA design pattern.
            </AccordionContent>
          </AccordionItem> */}
          </Accordion>
        </div>
        {/* <Button variant='secondary'>Hello</Button>
      <SearchInput
        options={optionsData}
        open={open}
        onOpenChange={setOpen}
        setOpen={setOpen}
        selectedValue={selectedValue}
        setSelectedValue={setSelectedValue}
        icon={true}
        placeholder='Select a company...'
        searchText='Search a company...'
        emptyMsg='No company found.'
        contentClassName='w-[500px]'
      />

      <InputAnimated type='password' /> */}

        {/* <RadioGroup defaultValue='option-one'>
        <div className='flex items-center space-x-2'>
          <RadioGroupItem value='option-one' id='option-one' />
          <Label className='font-normal' htmlFor='option-one'>
            Option One
          </Label>
        </div>
        <div className='flex items-center space-x-2'>
          <RadioGroupItem value='option-two' id='option-two' />
          <Label className='font-normal' htmlFor='option-two'>
            Option Two
          </Label>
        </div>
      </RadioGroup> */}
      </main>
    </>
  );
}
