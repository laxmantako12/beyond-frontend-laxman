'use client';

import SearchInput, { SelectOption } from '@/components/search-input';
import { Label } from '@/components/ui/label';
import { usePathname } from 'next/navigation';
import React from 'react';
import { optionsData, sidebarLinks } from './data';
import Link from 'next/link';
import { cn } from '@/lib/utils';
import { Icons } from '@/components/icons';
import { Badge } from '@/components/ui/badge';
import { Card } from '@/components/ui/card';

const Sidebar = () => {
  const [open, setOpen] = React.useState(false);
  const [selectedValue, setSelectedValue] = React.useState<SelectOption>();
  const pathname = usePathname();

  const isStageLinkActive = pathname.includes('/admin/stages');

  return (
    <div className='max-w-[315px] w-full sticky top-0'>
      <Card className='pl-[27px] pr-[25px] rounded-none rounded-tl-[20px] pb-[33px] bg-backgroundSidebar'>
        <Label className='text-[#0B202B] text-sm block pt-[29px] pb-1'>
          Select Competition
        </Label>
        <SearchInput
          options={optionsData}
          open={open}
          onOpenChange={setOpen}
          setOpen={setOpen}
          selectedValue={selectedValue}
          setSelectedValue={setSelectedValue}
          icon={true}
          placeholder='Select a company...'
          searchText='Search a company...'
          emptyMsg='No company found.'
          contentClassName='w-full'
          valueClassName='w-[150px] truncate overflow-ellipsis'
        />
      </Card>

      <Card className='w-full px-[20px] rounded-none rounded-bl-[20px] mt-1 bg-backgroundSidebar pt-[14px] pb-[100px]'>
        <div className='grid gap-y-[27px]'>
          {sidebarLinks.map((item) => (
            <div key={item.title}>
              <Link
                className={cn(
                  'flex items-center gap-1.5 text-grey-dark font-medium',
                  {
                    'text-secondary': item.child?.length && isStageLinkActive,
                    'text-secondary font-medium': pathname === item.href,
                  }
                )}
                href={item.href}
              >
                <Icons.right_arrow />
                <span>{item.title}</span>
              </Link>

              <div
                className={cn(' pl-4 grid gap-y-2', {
                  'mt-[14px]': item.child?.length,
                })}
              >
                {item.child?.map((child) => (
                  <Link
                    href={child.href}
                    key={child.title}
                    className='flex items-center gap-2 transition-colors'
                  >
                    <Badge
                      variant={
                        pathname === child.href ? 'secondary' : 'default'
                      }
                    >
                      Stage {child.stage}
                    </Badge>
                    <span
                      className={
                        pathname === child.href
                          ? 'text-secondary font-medium'
                          : 'text-grey-dark'
                      }
                    >
                      {child.title}
                    </span>
                  </Link>
                ))}
              </div>
            </div>
          ))}
        </div>
      </Card>
    </div>
  );
};

export default Sidebar;
