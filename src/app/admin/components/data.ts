import { SelectOption } from '@/components/search-input';

export const optionsData: SelectOption[] = [
  {
    id: '9cb0e66a-9937-465d-a188-2c4c4ae2401f',
    name: 'Grammatical Standard English',
  },
  {
    id: '61eb0e32-2391-4cd3-adc3-66efe09bc0b7',
    name: 'Summarize for a 2nd grader',
  },
  {
    id: 'a4e1fa51-f4ce-4e45-892c-224030a00bdd',
    name: 'Text to command',
  },
  {
    id: 'cc198b13-4933-43aa-977e-dcd95fa30770',
    name: 'Q&A',
  },
];

interface SiderbarLinkType {
  title: string;
  href: string;
  child?: {
    href: string;
    title: string;
    stage: number;
  }[];
}
export const sidebarLinks: SiderbarLinkType[] = [
  {
    title: 'Stages',
    href: '/admin/stages/duplicate-entries',
    child: [
      {
        href: '/admin/stages/duplicate-entries',
        title: 'Duplicate entries',
        stage: 1,
      },
      {
        href: '/admin/stages/automatic-filtration',
        title: 'Automatic Filtration',
        stage: 2,
      },
      {
        href: '/admin/stages/reg-phase-evaluation',
        title: 'Reg Phase Evaluation',
        stage: 3,
      },
      {
        href: '/admin/stages/judging-phase-evaluation',
        title: 'Judging Phase Evaluation',
        stage: 4,
      },
      {
        href: '/admin/stages/public-voting',
        title: 'Public Voting',
        stage: 5,
      },
      {
        href: '/admin/stages/announce-winners',
        title: 'Announce Winners',
        stage: 6,
      },
      {
        href: '/admin/stages/announced-winners',
        title: 'Announced Winners',
        stage: 7,
      },
    ],
  },
  {
    title: 'Groups',
    href: '/admin/groups',
  },
  {
    title: 'Evaluation Overview',
    href: '/admin/evaluation-overview',
  },
  {
    title: 'Logout',
    href: '',
  },
];
