import { Card } from '@/components/ui/card';
import Sidebar from './components/sidebar';

const Layout = ({ children }: { children: React.ReactNode }) => {
  return (
    <div className='h-[80vh]'>
      <div className='flex items-start'>
        <Sidebar />
        <Card className='border-l-0 flex-1 bg-white px-6 pt-[33px] pb-6 sticky top-7 rounded-none rounded-r-[20px]  mb-[18px]'>
          {children}
        </Card>
      </div>
    </div>
  );
};

export default Layout;
