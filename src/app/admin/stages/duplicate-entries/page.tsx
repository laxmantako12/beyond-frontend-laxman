'use client';

import Link from 'next/link';
import { usePathname } from 'next/navigation';
import React from 'react';

const DuplicateEntriesPage = () => {
  const location = usePathname();
  console.log('🚀 ~ DuplicateEntriesPage ~ location:', location);
  return (
    <div>
      <Link href='/'>Participants (80)</Link>
    </div>
  );
};

export default DuplicateEntriesPage;
