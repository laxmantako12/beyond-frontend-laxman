'use client';

import { Suspense } from 'react';
import InfoContent from './components/info-content';
import MainLinksTab from './components/main-links-tab';
import StatusTabs from './components/status-tabs';

const JudgingPhaseEvaluationPage = () => {
  return (
    <div>
      <Suspense>
        <MainLinksTab />
      </Suspense>
      <InfoContent />
      <StatusTabs />
    </div>
  );
};

export default JudgingPhaseEvaluationPage;
