'use client';

import { Icons } from '@/components/icons';
import { Checkbox } from '@/components/ui/checkbox';
import { ColumnDef } from '@tanstack/react-table';

// This type is used to define the shape of our data.
// You can use a Zod schema here if you want.
export type Payment = {
  id: string;
  email: string;
  participantId: string;
  name: string;
  nationalId: string;
  mobile_number: string;
};

export const columns: ColumnDef<Payment>[] = [
  {
    id: 'select',
    // header: ({ table }) => (
    //   <Checkbox
    //     checked={
    //       table.getIsAllPageRowsSelected() ||
    //       (table.getIsSomePageRowsSelected() && 'indeterminate')
    //     }
    //     onCheckedChange={(value) => table.toggleAllPageRowsSelected(!!value)}
    //     aria-label='Select all'
    //   />
    // ),
    cell: ({ row }) => (
      <div className='w-[30px]'>
        <Checkbox
          checked={row.getIsSelected()}
          onCheckedChange={(value) => row.toggleSelected(!!value)}
          aria-label='Select row'
        />
      </div>
    ),
    enableSorting: false,
    enableHiding: false,
  },
  {
    accessorKey: 'participantId',
    header: () => (
      <p className='leading-4'>
        Participant&nbsp;ID/<span className='block leading-4'>Name</span>
      </p>
    ),
  },
  {
    accessorKey: 'email',
    header: () => (
      <p className='leading-4'>
        Email/<span className='block leading-4'>Mobile&nbsp;Number</span>
      </p>
    ),
  },
  {
    accessorKey: 'nationalId',
    header: 'National ID',
  },
  {
    accessorKey: 'email',
    header: () => 'Evaluators Score',
    cell: ({ row }) => {
      return <div className='max-w-[200px]'>{row.original.email}</div>;
    },
  },
  {
    accessorKey: 'judges_score',
    header: 'Judges Score',
  },
  {
    accessorKey: 'total_score',
    header: 'Total Score',
  },

  {
    id: 'actions',
    cell: ({ row }) => {
      const payment = row.original;

      return (
        <div className='flex justify-end'>
          <button>
            <Icons.view />
          </button>
        </div>
      );
    },
  },
];
