import { optionsData } from '@/app/admin/components/data';
import SearchInput, { SelectOption } from '@/components/search-input';
import { Button } from '@/components/ui/button';
import { DataTable } from '@/components/ui/data-table';
import React from 'react';
import { columns } from './columns';

const getData = [
  {
    id: '728ed52f',
    participantId: 'FID001',
    name: 'John Doe',
    email: 'm@example.com',
    nationalId: 'NID098765999',
    mobile_number: '+966  4400000099',
  },
  {
    id: '728ed52',
    participantId: 'FID001',
    name: 'John Doe',
    email: 'm@example.com',
    nationalId: 'NID098765999',
    mobile_number: '+966  4400000099',
  },
  {
    id: '728e2f',
    participantId: 'FID001',
    name: 'John Doe',
    email: 'm@example.com',
    nationalId: 'NID098765999',
    mobile_number: '+966  4400000099',
  },
  {
    id: '72d52f',
    participantId: 'FID001',
    name: 'John Doe',
    email: 'm@example.com',
    nationalId: 'NID098765999',
    mobile_number: '+966  4400000099',
  },
  {
    id: '728ed5f2f',
    participantId: 'FID001',
    name: 'John Doe',
    email: 'm@example.com',
    nationalId: 'NID098765999',
    mobile_number: '+966  4400000099',
  },
];

const AllTabContent = () => {
  const [open, setOpen] = React.useState(false);
  const [selectedValue, setSelectedValue] = React.useState<SelectOption>();

  return (
    <div>
      <SearchInput
        options={optionsData}
        open={open}
        onOpenChange={setOpen}
        setOpen={setOpen}
        selectedValue={selectedValue}
        setSelectedValue={setSelectedValue}
        icon={true}
        placeholder='Select by'
        searchText='Search...'
        emptyMsg='No company found.'
        contentClassName='ml-0 w-[338px]'
        valueClassName='w-[250px] truncate overflow-ellipsis'
      />

      <div className='flex gap-x-3 items-center mt-6 mb-5'>
        <Button size='sm' className='uppercase px-[30px]'>
          add to group
        </Button>
        <Button size='sm' className='uppercase px-[15px]'>
          Select for Next Stage
        </Button>
        <Button variant='outline' size='sm' className='uppercase px-[30px]'>
          REJECT
        </Button>
      </div>

      <DataTable columns={columns} data={getData} />
    </div>
  );
};

export default AllTabContent;
