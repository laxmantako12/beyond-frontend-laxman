import { Button } from '@/components/ui/button';
import { cn } from '@/lib/utils';
import Link from 'next/link';
import { usePathname, useSearchParams } from 'next/navigation';
import React from 'react';

const MainLinksTab = () => {
  const location = usePathname();
  const params = useSearchParams();
  return (
    <div className='flex justify-between items-center  mb-5'>
      <div className='flex gap-12 items-center'>
        <Link
          className={cn(
            'font-extrabold uppercase tracking-[4.2px] text-primary-foreground',
            {
              'text-primary ': params.get('type') === 'participants',
            }
          )}
          href={`${location}?type=participants`}
        >
          Participants (80)
        </Link>
        <Link
          className={cn(
            'font-extrabold uppercase tracking-[4.2px] text-primary-foreground',
            {
              'text-primary ': params.get('type') === 'rejected',
            }
          )}
          href={`${location}?type=rejected`}
        >
          Rejected (30)
        </Link>
      </div>

      <Button
        variant='outline'
        size='sm'
        className='uppercase px-[30px] text-[10px] border-primary-foreground'
      >
        export
      </Button>
    </div>
  );
};

export default MainLinksTab;
