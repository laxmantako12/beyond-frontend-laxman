import { Card, CardContent } from '@/components/ui/card';
import { Label } from '@/components/ui/label';
import React from 'react';

const InfoCard = ({ title, value }: { title: string; value: any }) => {
  return (
    <CardContent className='p-0 flex flex-col font-medium'>
      <Label className='text-[#535E64] text-xs max-w-fit'>{title}</Label>
      <span className='text-primary-normal block max-w-fit'>{value}</span>
    </CardContent>
  );
};

const InfoContent = () => {
  return (
    <Card className='flex py-3 px-5 justify-between gap-4 flex-wrap'>
      <InfoCard title='Round Name' value='Round 2 - Vertual Demo Day' />
      <InfoCard title='Judging starts at' value='July 03, 2023' />
      <InfoCard title='Pending Submissions' value='57' />
      <InfoCard title='Completed Submissions' value='0' />
    </Card>
  );
};

export default InfoContent;
