import { Tabs, TabsContent, TabsList, TabsTrigger } from '@/components/ui/tabs';
import AllTabContent from './all-tab-content';
import CompletedTabContent from './completed-tab-content';
import PendingTabContent from './pending-tab-content';

const StatusTabs = () => {
  return (
    <div className='mt-5'>
      <Tabs defaultValue='all' className='w-full '>
        <TabsList className='flex justify-between bg-transparent p-0 gap-2'>
          <TabsTrigger value='all'>All (80) </TabsTrigger>
          <TabsTrigger value='pending'>Pending (40)</TabsTrigger>
          <TabsTrigger value='completed'>Completed (40)</TabsTrigger>
        </TabsList>
        <TabsContent value='all'>
          <AllTabContent />
        </TabsContent>
        <TabsContent value='pending'>
          <PendingTabContent />
        </TabsContent>
        <TabsContent value='completed'>
          <CompletedTabContent />
        </TabsContent>
      </Tabs>
    </div>
  );
};

export default StatusTabs;
